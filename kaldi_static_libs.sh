#!/usr/bin/env bash

if [[ "$1" == "Release" ]]; then
    mkdir -p ../lib/Release/
elif [[ "$1" == "Debug" ]]; then
    mkdir -p ../lib/Debug/
else
        echo "Usage:"
        echo "    \'kaldi_static_libs.sh Release\' for release mode"
    echo "    \'kaldi_static_libs.sh Debug\' for debug mode"
        exit 1
fi

for i in $(ls)
do
        output=$(echo $(ls $i | grep "\.a"))

        if [[ -n "$output" ]]; then
                cp $i/$output ../lib/$1/lib$output
        fi
done

