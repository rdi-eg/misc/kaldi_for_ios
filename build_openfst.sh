#!/usr/bin/env bash

set -e

if [[ ! -d kaldi ]]; then
    git clone https://github.com/kaldi-asr/kaldi.git
fi

cd kaldi/tools

default_ios_version=16.2

rm -rf openfst*

export SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk

# Set up relevant environment variables
export CPPFLAGS="-I$SDKROOT/usr/include/c++/4.2.1/ -I$SDKROOT/usr/include/ -miphoneos-version-min=12.0 -arch arm64 -fvisibility=hidden -fembed-bitcode"
export CFLAGS="$CPPFLAGS -arch arm64 -pipe -no-cpp-precomp -isysroot $SDKROOT"
export CXXFLAGS="$CFLAGS"

export LIB_SUFFIX=IOS
export LIBDIR=`pwd`/iOS-lib

OPENFST_VERSION=$(grep OPENFST_VERSION < Makefile | head -1 | cut -d' ' -f 3)
ln -s openfst-$OPENFST_VERSION openfst

make openfst-$OPENFST_VERSION

cd openfst

# apply patch to remove tests and regenerate openfst configure
patch configure.ac ../../../config.patch
autoreconf -ivf

./configure --program-suffix=LIB_SUFFIX  --disable-shared \
			--enable-static --disable-bin CXX=`xcrun -sdk iphoneos$default_ios_version -find g++` \
			CC=`xcrun -sdk iphoneos$default_ios_version -find gcc` \
			LD=`xcrun -sdk iphoneos$default_ios_version -find ld` \
			--host=armv8a-apple-darwin10

if [[ $? != 0 ]]
then
    echo "ERROR: Quiting because of problems with running ./configure"
    exit 1
fi

make -j $(sysctl -n hw.logicalcpu)

if [[ $? != 0 ]]
then
    echo "ERROR: Problems building openfst library"
    exit 1
else
	mkdir -p lib
    cp src/lib/.libs/libfst.a lib/libfst.a
    cp -r src/include include/
    echo
    echo "ios library is in lib/libfst.a. include files are under include/"
    echo
fi
