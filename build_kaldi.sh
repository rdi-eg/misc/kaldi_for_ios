#!/usr/bin/env bash

# ios-configure runs a "configure" script using the iOS 4.3 SDK, generating a
# static library that will load and run on your choice of iPhone, iPad, and
# their respective simulators.
#
# Simply run in the same directory as a "configure" script.
# You can run this script for multiple targets and use lipo(1) to stitch them
# together into a universal library.
#
# Collected and maintained by Nolan Waite (nolan@nolanw.ca)
#
# Magic compiler flags and incantations by Michael Aaron Safyan
# (michaelsafyan@gmail.com). Generality by Christopher J. Stawarz
# (http://pseudogreen.org/bzr/sandbox/iphone/build_for_iphoneos)
#
# arm64 iOS 13.6 support on macOS Catalina using Apple Accelerate
# instead of OpenBLAS by Adham Zahran (adhamzahranfms@gmail.com)

set -e

if [[ ! -d kaldi ]]; then
    git clone https://github.com/kaldi-asr/kaldi.git
fi

default_gcc_version=4.2.1
default_ios_version=16.2
default_min_ios_version=12.0
default_macosx_version=10.10

GCC_VERSION="${GCC_VERSION:-$default_gcc_version}"
export IOS_VERSION="${IOS_VERSION:-$default_ios_version}"
export MIN_IOS_VERSION="${MIN_IOS_VERSION:-$default_min_ios_version}"
export MACOSX_VERSION="${MACOSX_VERSION:-$default_macosx_version}"

DEVELOPER=`xcode-select -print-path`

usage ()
{
  cat >&2 << EOF
Usage: ${0##*/} [-h] [-p prefix] target [configure_args]
  -h      Print help message
  -p      Installation prefix
          (default: `pwd`/build/[target]-[version])

The target must be one of "iphone", "ipad", or "simulator". Any additional
arguments are passed to configure.

The following environment variables affect the build process:

  GCC_VERSION           (default: $default_gcc_version)
  IOS_VERSION           (default: $default_ios_version)
  MIN_IOS_VERSION       (default: $default_min_ios_version)
  MACOSX_VERSION        (default: $default_macosx_version)

EOF
}

while getopts ":hp:t" opt; do
    case $opt in
        h  ) usage ; exit 0 ;;
        p  ) prefix="$OPTARG" ;;
        \? ) usage ; exit 2 ;;
    esac
done
shift $(( $OPTIND - 1 ))

if (( $# < 1 )); then
    usage
    exit 2
fi

target=$1
shift

case $target in
    iphone )
        arch=arm64
        platform=iPhoneOS
        host=armv8a-apple-darwin10
        ;;

    ipad )
        arch=arm64
        platform=iPhoneOS
        host=armv8a-apple-darwin10
        ;;

    simulator )
        arch=x86_64
        platform=iPhoneSimulator
        host=x86_64-apple-darwin10
        ;;
    * )
        usage
        exit 2
esac

export DEVROOT="/$DEVELOPER/Platforms/${platform}.platform/Developer"
export SDKROOT="$DEVROOT/SDKs/${platform}${IOS_VERSION}.sdk"
prefix="${prefix:-`pwd`/build/${target}-${IOS_VERSION}}"

rm -rf usr
mkdir usr

# symlinking the Apple Accelerate BLAS headers to be used instead of OpenBLAS
ln -s $SDKROOT/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Headers/ usr/include

cd kaldi/src

if [ ! \( -d "$DEVROOT" \) ] ; then
   echo "The iPhone SDK could not be found. Folder \"$DEVROOT\" does not exist."
   exit 1
fi

if [ ! \( -d "$SDKROOT" \) ] ; then
   echo "The iPhone SDK could not be found. Folder \"$SDKROOT\" does not exist."
   exit 1
fi

if [ ! \( -x "./configure" \) ] ; then
    echo "This script must be run in the folder containing the \"configure\" script."
    exit 1
fi

export PKG_CONFIG_PATH="$SDKROOT/usr/lib/pkgconfig"
export PKG_CONFIG_LIBDIR="$PKG_CONFIG_PATH"
export AS="$DEVROOT/usr/bin/as"
export ASCPP="$DEVROOT/usr/bin/as"
export AR="$DEVROOT/usr/bin/ar"
export RANLIB="$DEVROOT/usr/bin/ranlib"
export CPPFLAGS="-O3 -miphoneos-version-min=${MIN_IOS_VERSION} -pipe -no-cpp-precomp -I$SDKROOT/usr/include -fvisibility=hidden -fembed-bitcode"
export CFLAGS="$CPPFLAGS -std=c99 -arch ${arch} -isysroot $SDKROOT -isystem $SDKROOT/usr/include"

# the first -isystem is added because of this: https://stackoverflow.com/questions/58628377/catalina-c-using-cmath-headers-yield-error-no-member-named-signbit-in-th
export CXXFLAGS="$CPPFLAGS -arch ${arch} -isystem `xcode-select -p`/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1 -isysroot $SDKROOT -isystem $SDKROOT/usr/include"

export LDFLAGS="-miphoneos-version-min=${MIN_IOS_VERSION} -arch ${arch} -isysroot $SDKROOT -L$SDKROOT/usr/lib -L$DEVROOT/usr/lib"
export CPP="$DEVROOT/usr/bin/cpp"
export CXXCPP="$DEVROOT/usr/bin/cpp"
export STRIP=`xcrun -sdk iphoneos13.6 -find strip`
export CXX=`xcrun -sdk iphoneos13.6 -find g++`
export CC=`xcrun -sdk iphoneos13.6 -find gcc`
export LD=`xcrun -sdk iphoneos13.6 -find ld`
export RANLIB=`xcrun -sdk iphoneos13.6 -find ranlib`
export AR=`xcrun -sdk iphoneos13.6 -find ar`

./configure \
    --static \
    --openblas-root=../../usr/

# disables the tests
# https://github.com/kaldi-asr/kaldi/issues/4137
sed -i.bak 's|TESTFILES|#TESTFILES|' base/Makefile
sed -i.bak 's|TESTFILES|#TESTFILES|' matrix/Makefile

make clean -j $(sysctl -n hw.logicalcpu)
make depend -j $(sysctl -n hw.logicalcpu)
make -j $(sysctl -n hw.logicalcpu)

echo "Copying kaldi libs to kaldi/lib/Release/"
bash ../../kaldi_static_libs.sh Release
