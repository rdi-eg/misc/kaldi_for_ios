# Kaldi for iOS

* Will clone and build kaldi for iOS 16.2
* We will use the Apple Accelerate BLAS implementation. So when you link your application make sure link to the Accelerate framework.
* bitcode is enabled

## Usage

* `./build_openfst.sh`
* `./build_kaldi.sh iphone`

## Result

* The resulting kaldi static libs will be at `kaldi/lib/Release/`
* openfst will be at `kaldi/tools/openfst/lib/`
* Kaldi headers is in each respective module folder. Example: `kaldi/src/base/`
* openfst headers will be at `kaldi/tools/openfst/include/`
